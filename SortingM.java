public class SortingM{
    public static void main(String []args){
    Scanner sc = new Scanner(System.in);
    Merge msort = new Merge();
    int times=100;
    double [] data= new double [times];
    
    while(sc.hasNext()){
        int size = sc.nextInt();
        int[] a = new int[size];
        long prom = 0;
        for(int j = 0; j < size; j++)
            a[j] = sc.nextInt();
        Statistics s = null;
        int maxTry = 1000; 
        double min=Double.MAX_VALUE;
        double bestEstimated=0;
         do{
            int [] b = new int[size];
            System.arraycopy(a,0,b,0,a.length);
            long estimatedTime = 0;
            for(int i=0;i<times;i++){
                long startTime= System.nanoTime();
                Merge.MergeSort(b);
                estimatedTime=System.nanoTime()-startTime;
                data[i]=estimatedTime;
            }
            s=new Statistics(data);
            maxTry--;
            if(s.getVariance()<min){
                min=s.getVariance();
                bestEstimated=estimatedTime;
            }
        }while(maxTry>0 && s.getVariance()>=5);
                System.out.println(size+" "+s.getMean());
        
      }  
    }
}
