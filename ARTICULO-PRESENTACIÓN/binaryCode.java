import java.util.*;
public class binaryCode{
  public static int search(int[]a,int elem){
      int left = 0;
      int right = a.length - 1;
      while (left <= right) {
          int mid = left + (right - left) / 2;
          if (elem < a[mid]) 
            right = mid - 1;
          else if (elem > a[mid]) 
            left = mid + 1;
          else 
            return mid;
      }
      return -1;
  }
}
