import java.util.*;
public class palindromeCode{
    public static boolean isPalindrome(String cadena){
      int right = cadena.length()-1;
      int left = 0;
      while(left<right && cadena.charAt(left)==cadena.charAt(right)){
            left+=1;
            right-=1;
      }
      if(left>=right)
        return true;
      else
        return false;
   }
}