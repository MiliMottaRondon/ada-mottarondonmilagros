import java.util.*;
public class wordPalin{
  public static void main(String []args){
    Scanner sc = new Scanner(System.in);
    palindromeCode pali = new palindromeCode();
    int times=100;
    double [] data= new double [times];
    
    while(sc.hasNext()){
        int size = sc.nextInt();
        char[] a = new char[size];
        String word = sc.next();
        long prom = 0;
        for(int j = 0; j < size; j++)
            a[j] = word.charAt(j);       
        Statistics s = null;
        int maxTry = 1000; 
        double min=Double.MAX_VALUE;
        double bestEstimated=0;
        do{
            char [] b = new char[size];
            System.arraycopy(a,0,b,0,a.length);
            long estimatedTime = 0;
            for(int i=0;i<times;i++){
                long startTime= System.nanoTime();
                boolean isornot = palindromeCode.istPalindrom(b);
                estimatedTime=System.nanoTime()-startTime;
                data[i]=estimatedTime;
            }
            s=new Statistics(data);
            maxTry--;
            if(s.getVariance()<min){
                min=s.getVariance();
                bestEstimated=estimatedTime;
            }
        }while(maxTry>0 && s.getVariance()>=5);
        System.out.println(size+" "+s.getMean());
    }  
  }
}
