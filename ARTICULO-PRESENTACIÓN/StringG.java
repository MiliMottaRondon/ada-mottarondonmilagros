import java.util.*;
import java.io.*;
public class StringG{
    private static String arrayString(int size){
        char[]letras={'a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z'};
        String[] array = new String[size];
        String palabra="";
        int indexR= (int)(Math.random()*(size-1)+1);
        for(int j=0;j<size;j++){
            palabra+=letras[indexR];                
        }    
        return palabra;
    }
    public static void print(PrintStream out, String a){
        out.print(a + " ");
        out.println();
    }
    public static void main(String[] args){
        if(args.length<3){
            System.err.println("Usage: $java ArrayGenerator <array size> <[repetitionsdefault is 1]>");
          System.exit(1);
        }
        int min=Integer.parseInt(args[0]);
        int max=Integer.parseInt(args[1]);
        int delta=Integer.parseInt(args[2]);

        for(int i=min; i<=max; i+=delta){
            System.out.println(i);
            print(System.out,arrayString(i));
        }
    }

}
