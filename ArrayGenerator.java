import java.util.*;
import java.io.*;
public class ArrayGenerator{
    private static int[] generateArray(int size){
    int[] array = new int[size];
    /*List<Integer> l = new ArrayList<Integer>();
    for(int i = 0; i < size; i++)
      l.add(i+1);
    for(int i = 0; i < size; i++){
      int idx = (int)(Math.random() * (size - i));
      array[i] = l.remove(idx);
    }
    return array;*/
    for(int i =size;i>0;i--)
        array[size-i]=i;
    return array;
  }
  public static void print(PrintStream out, int[] a){
    int i;
    for(i = 0; i < a.length - 1; i++)
      out.print(a[i] + " ");
    out.println(a[i]);
  }
  public static void main(String[] args){
    /*if(args.length == 0){
      System.err.println("Usage: $java ArrayGenerator <array size> <[repetitionsdefault is 1]>");
      System.exit(1);
    }

    int arraySize = Integer.parseInt(args[0]);
    int repetitions = args.length == 1 ? 1: Integer.parseInt(args[1]);

    System.out.println(arraySize + " " + repetitions);
    for(int i = 0; i < repetitions; i++){
      print(System.out, generateArray(arraySize));
    }*/
    if(args.length<3){
        System.err.println("Usage: $java ArrayGenerator <array size> <[repetitionsdefault is 1]>");
      System.exit(1);
    }
    int min=Integer.parseInt(args[0]);
    int max=Integer.parseInt(args[1]);
    int delta=Integer.parseInt(args[2]);

    for(int i=min; i<=max; i+=delta){
        System.out.println(i);
        print(System.out,generateArray(i));
    }

  }

}

