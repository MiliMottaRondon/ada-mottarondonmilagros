import java.util.*;
public class Stranssen{
	//for strassen we need add of matrix
	public static int[][] suma (int[][]a,int[][]b){
		if((a.length==b.length) && (a[0].length==b[0].length)){
			int [][]sum=new int[a.length][a[0].length];
			for (int i=0;i<sum.length;i++){
				for (int j=0;j<sum[i].length;j++) {
					sum[i][j]=a[i][j]+b[i][j];
				}				
			}
			return sum;

		}else
			return null;		
	}
	//for strassen we need sustraction of matrix
	public static int[][] resta (int[][]a,int[][]b){
		if((a.length==b.length) && (a[0].length==b[0].length)){
			int [][]dif=new int[a.length][a[0].length];
			for (int i=0;i<dif.length;i++){
				for (int j=0;j<dif[i].length;j++) {
					dif[i][j]=a[i][j]-b[i][j];
				}				
			}
			return dif;

		}else
			return null;		
	}
	public static int [][] strassen(int [][] a, int [][] b) {
		// We try to do strassen with the instruction of the book.
        	int n = a.length;
	        int [][] mult = new int[n][n];
	        if(n == 1) {
         	    mult[0][0] = a[0][0] * b[0][0];
	        } else {
			int [][] a11 =new int [n/2][n/2];
			int [][] a12 =new int [n/2][n/2];
			int [][] a21 =new int [n/2][n/2];
			int [][] a22 =new int [n/2][n/2];

			int [][] b11 =new int [n/2][n/2];
			int [][] b12 =new int [n/2][n/2];
			int [][] b21 =new int [n/2][n/2];
			int [][] b22 =new int [n/2][n/2];
		    
		 	dividir(a, a11, 0 , 0);
		        dividir(a, a12, 0 , n/2);
	        	dividir(a, a21, n/2, 0);
	            	dividir(a, a22, n/2, n/2);

        	    	dividir(b, b11, 0 , 0);
	            	dividir(b, b12, 0 , n/2);
	            	dividir(b, b21, n/2, 0);
	            	dividir(b, b22, n/2, n/2);
			
			int [][] p1 = strassen(suma(a11, a22), suma(b11, b22));
			int [][] p2 = strassen(suma(a21, a22), b11);
			int [][] p3 = strassen(a11, resta(b12, b22));
			int [][] p4 = strassen(a22, resta(b21, b11));
			int [][] p5 = strassen(suma(a11, a12), b22);
			int [][] p6 = strassen(resta(a21, a11), suma(b11, b12));
			int [][] p7 = strassen(resta(a12, a22), suma(b21, b22));

			int [][] c11 = suma(resta(suma(p1, p4), p5), p7);
			int [][] c12 = suma(p3, p5);
			int [][] c21 = suma(p2, p4);
			int [][] c22 = suma(resta(suma(p1, p3), p2), p6);

			juntarM(c11, mult, 0 , 0);
			juntarM(c12, mult, 0 , n/2);
			juntarM(c21, mult, n/2, 0);
			juntarM(c22, mult, n/2, n/2);
		    
        	}
        	return mult;
        }
        public static void dividir(int[][] original, int[][] nuevo, int i, int j) {
		for(int i1 = 0, i2=i; i1<nuevo.length; i1++, i2++){
                     	for(int j1 = 0, j2=j; j1<nuevo.length; j1++, j2++){
                        	nuevo[i1][j1] = original[i2][j2];
                     	}
                }
        }
	
	public static void juntarM(int[][] nuevo, int[][] original, int ib, int jb) {
		for(int i1 = 0, i2=ib; i1<nuevo.length; i1++, i2++){
			for(int j1 = 0, j2=jb; j1<nuevo.length; j1++, j2++){
				original[i2][j2] = nuevo[i1][j1];
			}
		}
	}
}
