import java.util.*;
public class TwoMult{
    public static void main(String []args){
	Scanner sc = new Scanner(System.in);
	MultiplicationM regular = new MultiplicationM();
	Stranssen stran = new Stranssen();
	int times=100;
	double [] dataReg = new double [times];
	double [] dataStr = new double [times];
        
	while(sc.hasNext()){
		int size = sc.nextInt();            
		int[][] a = new int[size][size];
		int[][] b = new int[size][size];
		int[][] reguResult=new int[size][size];
		int[][] straResult=new int[size][size];
		for(int i=0;i<size;i++){
			for(int j = 0; j < size; j++){
				a[i][j] = sc.nextInt();
			}
		}
		size = sc.nextInt();
		for(int i=0;i<size;i++){
			for(int j = 0; j < size; j++){
				b[i][j] = sc.nextInt();
			}
		}
		Statistics sReg = null;
		Statistics sStr = null;

		int maxTry = 1000; 
		double minR=Double.MAX_VALUE;
		double minS=Double.MAX_VALUE;
		double bestEstimatedR=0;
		double bestEstimatedS=0;
		do{
			long estimatedTime = 0;
			long estimatedTimeS = 0;

			for(int i=0;i<times;i++){
				//Para Multiplicacion Regular
				long startTime= System.nanoTime();
				reguResult=MultiplicationM.mult(a,b);
				estimatedTime=System.nanoTime()-startTime;
				dataReg[i]=estimatedTime;
				//Para Stranssen
				long startTimeS= System.nanoTime();
				straResult=Stranssen.strassen(a,b);
				estimatedTimeS=System.nanoTime()-startTimeS;
				dataStr[i]=estimatedTimeS;
			}
			sReg=new Statistics(dataReg);
			sStr=new Statistics(dataStr);
			maxTry--;
			//Para Multiplicacion Regular
			if(sReg.getVariance()<minR){
				minR=sReg.getVariance();
				bestEstimatedR=estimatedTime;
			}
			//Para Stranssen
			if(sStr.getVariance()<minS){
				minS=sStr.getVariance();
				bestEstimatedS=estimatedTimeS;
			}
		}while(maxTry>0 && (sReg.getVariance()>=5 || sStr.getVariance()>=5));
			System.out.println(size+" "+sReg.getMean());
			System.out.println(size+" "+sStr.getMean());
		}  
	}
}
