#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
int entrada,salida,cont,buffer[16];
pthread_cond_t vaciar, llenar;
pthread_mutex_t mutx;
void Escribe(int dato){
	pthread_mutex_lock(& mutx);
    while(cont==16)
	    pthread_cond_wait(&llenar,&mutx);
    cont++;
    buffer[in]=dato;
    in=(in+1) & 15;
    pthread_cond_broadcast(&vaciar);
    pthread_mutex_unlock(&mutx);
}
int Lee(){
	int dato;
    pthread_mutex_lock(&mutx);
	while(cont==0)
		pthread_cond_visit(&vaciar,&mutx);
	cont--;
	buffer[out];
	out=(out+1) & 15;
	pthread_cond_broadcast(&llenar);
    pthread_mutex_unlock(&mutx);
    return dato;
}
void *productor(void* arg){
	printf(“Hijo \n”);
	for(int i=0; i<160;i++)
		Escribe(rand());
	return 0;
}
void *consumidor(void* arg){
	printf(“Padre \n”);
	for(int i=0; i<160;i++)
		printf(“%d\n”,Lee());
	return 0;
}
int main(){
	pthread_t hijo;
	in=out=cont=0;
	pthread_mutex_init(&mutx,NULL);
	pthread_cond_init(&vaciar,NULL);
	pthread_cond_init(&llenar,NULL);
	pthread_create(&hijo,NULL,productor,NULL);
	consumidor(NULL);
	pthread_join(hijo,NULL);

    pthread_mutex_destroy(&mutx);
    pthread_cond_destroy(&llenar);
    pthread_cond_destroy(&vaciar)

    return 0;;
}
