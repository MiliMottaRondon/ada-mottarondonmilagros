import java.util.*;
import java.io.*;
public class Merge{
	public static int[] MergeSort(int[]a,int inicio,int fin){
		if(inicio<fin){
			int q=(inicio+fin)/2;
			MergeSort(a,inicio,q);
			MergeSort(a,q+1,fin);
			return Merge(a,inicio,q,fin);
		}
		return a;

	}
	public static int[] Merge (int[]A,int inicio,int medio,int fin){
		int[]temporal=new int[A.length];
		for (int i = inicio;i <= fin; i++) {
			temporal[i] = A[i];
		}
		int i = inicio;
		int j = medio + 1;
		int k = inicio;
		while (i <= medio && j <= fin) {
			if (temporal[i] <= temporal[j]) {
				A[k] = temporal[i];
				i++;
			} else {
				A[k] = temporal[j];
				j++;
			}
			k++;
		}
		while (i <= medio) {
			A[k] = temporal[i];
			k++;
			i++;
		}
		return A;
	}
}
