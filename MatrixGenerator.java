import java.util.*;
import java.io.*;
public class MatrixGenerator{
	public static void main (String[]args){
		if(args.length<3){
		    System.err.println("Usage: $java MatrixGenerator <dimension inicio> <dimension final> <[repetitionsdefault is 1]>");
		    System.exit(1);
		}
		int min=Integer.parseInt(args[0]);
		int max=Integer.parseInt(args[1]);
		int delta=Integer.parseInt(args[2]);

		for(int i=min; i<=max; i+=delta){
		    print(System.out,matrixG(i));
		    print(System.out,matrixG(i));
	 	}
	}
	public static int[][] matrixG(int size){
		int[][]matrix =new int[size][size];
		for(int i=0;i<size;i++){
			for(int j=0;j<size;j++){
				//Lleno la matriz con numeros aleatorios entre 1 y size
				matrix[i][j]=(int)(Math.random()*(size-1)+1);
			}
		}
		return matrix;
	}
	public static void print(PrintStream out, int[][] a){
	    	int i,j;
	    	out.print(a.length+"\n");
	    	for(i = 0; i < a.length; i++){
	    		for(j=0;j<a[i].length;j++)
	    			out.print(a[i][j] + " ");
	    		out.println();
	    	}
	}
}
